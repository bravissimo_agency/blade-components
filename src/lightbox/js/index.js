const components = {
    LightboxProvider: () =>
        import(
            /* webpackChunkName: "lightbox-provider" */ './LightboxProvider.vue'
        ),
};

export default (Vue) => {
    for (const name in components) {
        Vue.component(name, components[name])
    }
};