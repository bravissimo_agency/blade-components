@props([
    'images',
    'settings' => null
])

<lightbox-provider
    :images='@json($images)'
    :settings='@json($settings)'
    v-slot:default="LightboxProvider"
>
    {!! $slot !!}
</lightbox-provider>

@once
    @push('bottom')
        <x-lightbox::pswp />
    @endpush
@endonce