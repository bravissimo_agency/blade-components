import Vue from 'vue';
export const EventBus = new Vue();

const components = {
    DrawerProvider: () =>
        import(
            /* webpackChunkName: "drawer-provider" */ './DrawerProvider.vue'
        ),
    DrawerTrigger: () =>
        import(
            /* webpackChunkName: "drawer-trigger" */ './DrawerTrigger.vue'
        ),
};

export default (Vue) => {
    for (const name in components) {
        Vue.component(name, components[name])
    }
};