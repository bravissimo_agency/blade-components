<button
    type="button"
    {{ $attributes->merge(['title' => 'Stäng', 'aria-label' => 'Stäng']) }}
    @click="DrawerProvider.$close"
>
    {!! $slot !!}
</button>