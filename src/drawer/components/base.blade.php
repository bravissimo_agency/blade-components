@props([
    'name',
    'activeBodyClass' => null
])

<drawer-provider
    name="{{ $name }}"
    active-body-class="{{ $activeBodyClass }}"
    v-slot:default="DrawerProvider"
>
    <focus-trap :active="DrawerProvider.isOpen">
        <transition name="{{ $name }}">
            <div
                v-show="DrawerProvider.isOpen"
                v-bind:aria-hidden="DrawerProvider.isOpen ? 'false' : 'true'"
                id="{{ $name }}"
                role="region"
                v-esc="DrawerProvider.$close"
                tabindex="-1"
                v-cloak
                {{ $attributes->merge(['class' => 'drawer']) }}
            >
                {!! $slot !!}
            </div>
        </transition>
    </focus-trap>
</drawer-provider>
