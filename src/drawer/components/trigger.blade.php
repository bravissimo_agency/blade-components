@props([
    'id'
])

<drawer-trigger
    v-bind:drawer-id="'{{ $id }}'"
    v-slot:default="$trigger"
>
    {!! $slot !!}
</drawer-trigger>