@props([
    'noSelectedLabel' => null,
    'labelClass' => '',
    'iconClass' => ''
])

<dropdown-button-provider
    v-bind:active-item="DropdownProvider.activeItem"
    no-selected-label="{{ $noSelectedLabel ?? null }}"
    v-slot:default="item"
>
    <button
        v-bind:id="DropdownProvider.id + '__button'"
        v-bind:aria-labelledby="DropdownProvider.id + '__button'"
        v-bind:aria-expanded="DropdownProvider.isOpen.toString()"
        aria-haspopup="listbox"
        type="button"
        @click="DropdownProvider.$toggle"
        {{ $attributes->merge(['class' => 'dropdownButton']) }}
    >
        @if (!empty($before))
            {!! $before !!}
        @endif

        {!! $slot ?? null !!}

        @if (!empty($slot))
            <span class="dropdownButton__label {{ $labelClass }}">
                @if (!empty($label))
                    {!! $label !!}
                @elseif(!empty($noSelectedLabel))
                    <span v-text="item.label">
                        {{ $noSelectedLabel }}
                    </span>
                @else
                    <span v-text="item.label"></span>
                @endif
            </span>
        @endif

        @if (!empty($after))
            {!! $after !!}
        @else
            <svg 
                xmlns="http://www.w3.org/2000/svg" 
                width="24"
                height="24" 
                viewBox="0 0 24 24" 
                fill="none" 
                stroke="currentColor" 
                stroke-width="2" 
                stroke-linecap="round" 
                stroke-linejoin="round" 
                class="dropdownButton__chevron {{ $iconClass }} flex-shrink-0"
            ><polyline points="6 9 12 15 18 9"></polyline></svg>
        @endif
    </button>
</dropdown-button-provider>