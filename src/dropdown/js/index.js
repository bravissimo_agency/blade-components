const components = {
    DropdownProvider: () =>
        import(
            /* webpackChunkName: "dropdown-provider" */ './DropdownProvider.vue'
        ),
    DropdownButtonProvider: () =>
        import(
            /* webpackChunkName: "dropdown-button-provider" */ './DropdownButtonProvider.vue'
        ),
};

export default  {
    install (Vue) {
        Object.keys(components).forEach(name => {
            Vue.component(name, components[name]);
        });
    }
};
