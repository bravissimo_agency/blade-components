<?php

namespace Bravissimo\BladeComponents;

use Illuminate\Support\ServiceProvider;

class BladeComponentsServiceProvider extends ServiceProvider {
    public function boot() {
        $this->loadViewsFrom(__DIR__ . '/accordion', 'accordion');
        $this->loadViewsFrom(__DIR__ . '/carousel', 'carousel');
        $this->loadViewsFrom(__DIR__ . '/dialog', 'dialog');
        $this->loadViewsFrom(__DIR__ . '/drawer', 'drawer');
        $this->loadViewsFrom(__DIR__ . '/dropdown', 'dropdown');
        $this->loadViewsFrom(__DIR__ . '/form', 'form');
        $this->loadViewsFrom(__DIR__ . '/googleMaps', 'googleMaps');
        $this->loadViewsFrom(__DIR__ . '/lightbox', 'lightbox');
        $this->loadViewsFrom(__DIR__ . '/notification', 'notification');
    }
}
