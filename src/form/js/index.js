const components = {
    CheckboxGroupProvider: () =>
        import(
            /* webpackChunkName: "checkbox-group-provider" */ './CheckboxGroupProvider.vue'
        ),
    FormProvider: () =>
        import(
            /* webpackChunkName: "form-provider" */ './FormProvider.vue'
        ),
};

export default (Vue) => {
    for (const name in components) {
        Vue.component(name, components[name])
    }
};