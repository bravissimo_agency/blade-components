@props([
    'label'
])

<div
    role="checkbox"
    tabindex="0"
    {{ $attributes->merge(['class' => 'checkbox flex items-center']) }}
>
    <div class="checkbox__icon relative flex-shrink-0">
        <span class="checkbox__dot"></span>
    </div>
    
    <div class="checkbox__label">
        {!! $label ?? $slot !!}
    </div>
</div>
