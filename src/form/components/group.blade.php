@props([
    'labelBelow' => false,
    'label' => null,
    'name' => null
])

<div {{ $attributes->merge(['class' => 'formGroup']) }}>
    <div class="formGroup__inner">
        @if (!empty($label) && !$labelBelow)
            <x-form::label
                :for="$name"
                v-bind:class="{ 'hasError': FormProvider.errors && FormProvider.errors['{{ $name }}'] }"
                class="top"
            >
                {!! $label !!}
            </x-form::label>
        @endif

        {!! $slot !!}

        @if (!empty($label) && $labelBelow)
            <x-form::label
                :for="$name"
                v-bind:class="{ 'hasError': FormProvider.errors && FormProvider.errors['{{ $name }}'] }"
                class="top"
            >
                {!! $label !!}
            </x-form::label>
        @endif
    </div>
</div>
