@props([
    'endpoint',
    'formId',
    'initialFormData' => null,
    'gtmData' => null,
    'mustAcceptTerms' => null,
    'onSuccess' => null
])

<form-provider
    v-slot:default="FormProvider"
    form-id="{{ $formId }}"
    :initial-form-data='@json($initialFormData)'
    :gtm-data='@json($gtmData)'
    :must-accept-terms='@json($mustAcceptTerms)'
    error-message="{{ config('form.error_message') }}"
    terms-message="{{ config('form.terms_message') }}"
    endpoint="{{ $endpoint }}"
    @success="{{ $onSuccess ?? null }}"
>
    <form
        {{ $attributes->merge(['class' => 'form']) }}
        v-on:submit.prevent="FormProvider.$submitHandler"
    >
        {!! $slot !!}
    </form>
</form-provider>
