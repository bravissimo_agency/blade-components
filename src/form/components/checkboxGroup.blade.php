@props([
    'items',
    'checkboxClass' => ''
])

<checkbox-group-provider
    v-slot:default="CheckboxGroupProvider"
    {{ $attributes->only('v-model') }}
>
    <div
        class="checkboxGroup"
        role="group"
        {{ $attributes->except('v-model') }}
    >
        @foreach ($items as ['value' => $value, 'label' => $label])
            <x-form::checkbox
                :label="$label"
                v-bind:aria-checked="CheckboxGroupProvider.$isActive('{{ $value }}')? 'true' : 'false'"
                v-bind:class="{ 'isActive': CheckboxGroupProvider.$isActive('{{ $value }}')}"
                :class="$checkboxClass"
                @click="CheckboxGroupProvider.$clickHandler('{{ $value }}')"
                @keydown.prevent.enter.space="CheckboxGroupProvider.$clickHandler('{{ $value }}')"
            />
        @endforeach
    </div>
</checkbox-group-provider>