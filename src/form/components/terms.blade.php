<x-form::checkbox
    v-bind:aria-checked="FormProvider.termsAccepted ? 'true' : 'false'"
    v-bind:class="{ 'isActive': FormProvider.termsAccepted }"
    @click="FormProvider.$toggleTerms"
    @keydown.prevent.enter.space="CheckboxGroupProvider.$toggleTerms"
>
    {!! $slot !!}
</x-form::checkbox>