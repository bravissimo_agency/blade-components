import Vue from 'vue';
export const EventBus = new Vue();

Vue.prototype.$notification = notification =>
    EventBus.$emit('addNotification', notification);
