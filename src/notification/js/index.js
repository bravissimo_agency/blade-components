import NotificationContainer from './NotificationContainer.vue';
import { EventBus } from './eventBus';

const NotificationComponents = {
    NotificationContainer
};

const NotificationPlugin = {
    install (Vue) {
        Vue.prototype.$notification = Notification => EventBus.$emit('addNotification', Notification);

        Object.keys(NotificationComponents).forEach(name => {
            Vue.component(name, NotificationComponents[name]);
        });
    }
};

export default NotificationPlugin;
