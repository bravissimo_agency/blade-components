@props([
    'cookieMessage'
])

<notification-container
    cookie-message='{!! $cookieMessage ?? null !!}'
></notification-container>