const components = {
    CarouselProvider: () =>
        import(
            /* webpackChunkName: "carousel-provider" */ './CarouselProvider.vue'
        ),
};

export default (Vue) => {
    for (const name in components) {
        Vue.component(name, components[name])
    }
};