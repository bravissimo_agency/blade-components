@props([
    'settings' => null,
    'fullSize' => null,
    'slides'
])

<carousel-provider 
    :slides='@json($slides)'
    :settings='@json($settings)'
    v-slot:default="CarouselProvider"
>
    <div 
        {{ $attributes->merge(['class' => 'carousel ' . (!empty($fullSize) ? 'isFullSize' : '')]) }}
    >    
        {!! $slot !!}
    </div>
</carousel-provider>