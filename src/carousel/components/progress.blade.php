@props([
    'barClass' => 'carouselProgress__bar ab100',
    'pausedClass' => 'isPaused'
])

<div {{ $attributes->merge(['class' => 'carouselProgress']) }}>
    <div
        :key="CarouselProvider.currentSlide"
        :class="{ {{ $pausedClass }}: CarouselProvider.isPaused }"
        :style="{ animationDuration: CarouselProvider.timePerSlide + 'ms' }"
        class="{{ $barClass }}"
    ></div>
</div>