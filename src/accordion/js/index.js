const components = {
    AccordionProvider: () =>
        import(
            /* webpackChunkName: "accordion-provider" */ './AccordionProvider.vue'
        ),
};

export default (Vue) => {
    for (const name in components) {
        Vue.component(name, components[name])
    }
};