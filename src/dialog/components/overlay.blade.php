<dialog-close-provider>
    <div {{ $attributes }}>
        {!! $slot ?? null !!}
    </div>
</dialog-close-provider>